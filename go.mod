module github.com/skeema/mybase

go 1.18

require (
	github.com/mitchellh/go-wordwrap v1.0.0
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
)

require golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
